# INSTAL·LACIÓ DE MOODLE AMB DOCKER-COMPOSE

***Prerequisit***: Tenir instal·lat Docker.

Docker us ve preinstal·lat al Debian 11 de l’escola. Si ho volguéssiu instal·lar a casa, primer
haurieu d’instal·lar Docker.

El següent que farem és instal·lar Docker-compose.

>
>
>

Descripcion|Codigo
----|---
Primer descarregarem la última versió estable de docker-compose. Escriurem al terminal:| *$ sudo curl -L"https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose*
Donarem permisos al docker-compose: | *$ sudo chmod +x /usr/local/bin/docker-compose*
Comprovarem que hem instal·lat correctament docker-compose: | *$ sudo docker-compose --version*

Descarreguem el fitxer docker-compose.yml de moodle. El guardem a un directori que
tinguem permisos.

Un altre cop al terminal i dins del directori on tinguem descarregat el docker-compose.yml,
escriurem:

$ sudo docker-compose up -d

Un cop hagi acabat la instal·lació, obrirem un navegador i escriurem a la url localhost.

![localhost](Pictures/localhost.png)

Se’ns obrirà un moodle. Podem logar-nos fent servir les següents
credencials:

>- usuari: user
>
>- contrassenya: bitnami

Cada cop que apaguem l’ordinador haurem d’escriure el següent:

$ sudo docker ps -a

Ens llistarà els contenidors que tenim en marxa. Hem de reiniciar-los, escrivint:

$ sudo docker restart nom_del_contenidor

on nom_del_contenidor serà el nom que ens aparegui al fer sudo docker ps -a.

[MEMES](https://www.google.com/search?q=MEMES&sxsrf=AOaemvIm3HEDJuari9I4dtUY8hJDPbuV6A:1636371825995&source=lnms&tbm=isch&sa=X&ved=2ahUKEwignKKL2Ij0AhWPDxQKHaWdAsUQ_AUoAXoECAEQAw&biw=1920&bih=917&dpr=1)

![ll](index.jpeg)

[![lll](Downloads/download.png)](https://www.amazon.es/?&tag=hydesnav-21&ref=pd_sl_781oit2196_e&adgrpid=55589983189&hvpone=&hvptwo=&hvadid=275354669239&hvpos=&hvnetw=g&hvrand=11286354348082162807&hvqmt=e&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=1005424&hvtargid=kwd-10573980&hydadcr=4855_1809861&gclid=Cj0KCQiAsqOMBhDFARIsAFBTN3e-FrSkimnUwYEpPnwnUy2VZwlPczgIwVLo88CgZr9CuFbIVYP0REIaAu8WEALw_wcB)

![ii](/home/users/inf/jism1/ism39441167/download.jpeg)


In a [hole][1] in the ground there lived a [hobbit][2]. Not a nasty, dirty, wet hole, [filled][3] with the ends of worms and an oozy [smell][4], nor yet a dry, bare, sandy hole with [nothing][5] in it to sit down on or to eat: it was a hobbit-hole, and that means comfort.

[1]:http://www.escoladeltreball.org/ca/news/consell-escolar-professorat
[2]:http://www.escoladeltreball.org/ca/escola
[3]:http://www.escoladeltreball.org/ca/estudis
[4]:http://www.escoladeltreball.org/ca/secretaria
[5]:http://www.escoladeltreball.org/ca/serveis

- [X] hOLA
- [X] hOLA
- [X] hOLA
- [ ] hOLA
- [X] hOLA
- [X] hOLA









